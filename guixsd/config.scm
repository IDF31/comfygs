;; This is an operating system configuration generated
;; by the graphical installer.

(use-modules (gnu) (srfi srfi-1)
	     (gnu packages emacs)
	     (gnu packages emacs-xyz)
	     (gnu packages gcc)
	     (gnu packages wm)
	     (gnu packages xorg)
	     (guix packages)
	     (guix download)
	     (guix git-download)
	     (guix licenses)
	     (guix build-system trivial)
	     (gnu packages linux)
	     (gnu packages patchutils))

(use-service-modules desktop networking ssh xorg audio sound)

;(define %linux-pf-patchset
;  (let ((kernel-version "v5.4")
;	(patchset-version "pf3"))
;    (origin
;     (method url-fetch)
;     (uri
;     (string-append
;       "https://github.com/pfactum/pf-kernel/compare/"
;       kernel-version
;       "..."
;       kernel-version "-" patchset-version ".diff"))
;     (sha256
;      (base32 "1l2d7qp3x6gqvg7l8zsd6mqc9ir6jwi7ml99hf70xx55ckiacwyw")))))

(define-public zen-linux-nonfree
  (package
   (inherit linux-libre-5.4)
   (name "zen-linux-nonfree")
   (version "v5.4.6-zen3")
   (home-page "kernel.org")
   (description "The Zen Kernel is a the result of a collaborative effort of kernel hackers to provide the best Linux kernel possible for every day systems.")
   (synopsis "Linux kernel with various patches and features.")
   (source (origin
	    (method url-fetch)
	    (uri
	     (string-append
	      "https://github.com/zen-kernel/zen-kernel/archive/"
	      version
	      ".tar.gz"))
	    (sha256
	     (base32
	      "1s3x7q4gv88v5yvhvsiwnvfxilrk175q5233brljc3kx3sni9x7i"))))))

(define-public linux-firmware-nonfree
  (package
   (name "linux-firmware-nonfree")
   (version "eefb5f7410150c00d0ab5c41c5d817ae9bf449b3")
   (license non-copyleft)
   (home-page "https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git")
   (synopsis "Nonfree firmware for the Linux kernel")
   (build-system trivial-build-system)
   (arguments
     `(#:modules ((guix build utils))
       #:builder (begin
                   (use-modules (guix build utils))
                   (let ((source (assoc-ref %build-inputs "source"))
                         (fw-dir (string-append %output "/lib/firmware/")))
                     (mkdir-p fw-dir)
		     (copy-recursively source fw-dir)
                     #t))))
			      
   (description "Nonfree firmware to be loaded by driver modules")
   (source (origin
	    (method git-fetch)
	    (uri
	     (git-reference
	      (url "git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git")
	      (commit version)))
	    (sha256
	     (base32
	      "01zwmgva2263ksssqhhi46jh5kzb6z1a4xs8agsb2mbwifxf84cl"))))))

(operating-system
  (locale "ro_RO.utf8")
  (timezone "Europe/Bucharest")
  (keyboard-layout
   (keyboard-layout "us" "altgr-intl"))
  (kernel zen-linux-nonfree)
  (firmware (cons*
	     linux-firmware-nonfree
	     %base-firmware))
  (bootloader
    (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (target "/boot/efi")
      (keyboard-layout keyboard-layout)))
  (mapped-devices (list (mapped-device
     (source "/dev/sdb")
     (target "home")
     (type luks-device-mapping))))
  (file-systems
    (cons* (file-system
             (mount-point "/boot")
             (device
               (uuid "56aa836a-d8ff-4534-89f1-36b619bd19e0"
                     'ext4))
             (type "ext4"))
           (file-system
             (mount-point "/")
             (device
               (uuid "91a22ddd-e72b-4695-9e35-68af13a0c173"
                     'ext4))
             (type "ext4"))
           (file-system
             (mount-point "/boot/efi")
             (device (uuid "ECC5-141C" 'fat32))
             (type "vfat"))
           (file-system
             (mount-point "/home")
             (type "xfs")
             (device "/dev/mapper/home"))
             %base-file-systems))
  (host-name "lisp-machine")
  (users (cons* (user-account
                  (name "idf31")
                  (comment "Idf31")
                  (group "users")
                  (home-directory "/home/idf31")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))
  (services (append (list
		     (service mpd-service-type
			      (mpd-configuration
			       (user "idf31")
			       (music-dir "~/music")
			       (address "localhost")
			       (port "6600"))))
		    (cons* (service slim-service-type
				    (slim-configuration
				     (display ":0")
				     (vt "vt7")))
			   (remove (lambda (service)
				     (eq? (service-kind service) gdm-service-type))
				   %desktop-services))))
  (packages
   (append (list
            emacs emacs-exwm emacs-solarized-theme emacs-guix emacs-telega
	    stumpwm xterm
	    gcc
	    (specification->package "nss-certs"))
	   %base-packages)))
