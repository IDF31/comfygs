;; Basic initialisation stuff

(package-initialize)
(require 'package)
(setq package-archives
      '(("melpa" . "https://melpa.org/packages/")
	("gnu" . "https://elpa.gnu.org/packages/")))

(set-face-attribute 'default nil :font "Hack")
(set-frame-font "Hack" nil t)

;; EMMS configuration
(require 'emms-setup)
(emms-all)
(emms-default-players)
(require 'emms-player-mpd)
(setq emms-player-mpd-server-name "localhost")
(setq emms-player-mpd-server-port "6600")
(setq emms-player-mpd-music-directory "~/music")
(add-to-list 'emms-info-functions 'emms-info-mpd)
(add-to-list 'emms-player-list 'emms-player-mpd)
(global-set-key (kbd "<XF86AudioPlay>") 'emms-start)
(global-set-key (kbd "<XF86AudioNext>") 'emms-next)
(global-set-key (kbd "<XF86AudioPrev>") 'emms-previous)
(global-set-key (kbd "s-e b") 'emms-smart-browse)

;; Configure telega.el
(add-hook 'telega-chat-mode-hook
	  (lambda ()
	    (local-set-key (kbd "s-f") 'telega-msg-forward)
	    (local-set-key (kbd "s-r") 'telega-msg-reply)
	    (local-set-key (kbd "s-a") 'telega-chatbuf-attach)
	    (local-set-key (kbd "s-s") 'telega-stickerset-choose)))
(add-hook 'telega-root-mode-hook
	  (lambda ()
	    (local-set-key (kbd "s-c") 'telega-chat-with)))


;; Additional hooks
(add-hook 'emacs-lisp-mode-hook
	  (lambda ()
	    (local-set-key (kbd "C-c C-c") 'eval-buffer)))

;; Enwc config
(setq enwc-default-backend 'nm)

;; Ivy config
(require 'ivy)
(ivy-mode t)

;;Disable decorations
(tool-bar-mode 0)
(menu-bar-mode 0)
(scroll-bar-mode 0)

;; Configure mode line
(display-time-mode t)
(display-battery-mode t)
(nyan-mode t)
(setq nyan-animate-nyancat t)
(setq nyan-wavy-trail t)
(setq nyan-bar-length 12)
(setq nyan-minimum-window-width 70)
(nyan-start-animation)

;; Global key-binds declarations
(global-set-key (kbd "M-o") 'ace-window)
(global-set-key (kbd "<XF86AudioRaiseVolume>") 'amixer-up-volume)
(global-set-key (kbd "<XF86AudioLowerVolume>") 'amixer-down-volume)
(global-set-key (kbd "<XF86AudioMute>") 'amixer-toggle-mute)
(global-set-key (kbd "s-g s-g") 'guix)
(global-set-key (kbd "s-g s") 'guix-search-by-name)
(global-set-key (kbd "s-g p") 'guix-packages-by-name)
(global-set-key (kbd "s-g S") 'guix-services-by-name)
(global-set-key (kbd "s-g e") 'guix-edit)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-quickhelp-color-background "#4F4F4F")
 '(company-quickhelp-color-foreground "#DCDCCC")
 '(custom-enabled-themes (quote (solarized-dark)))
 '(custom-safe-themes
   (quote
    ("a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e" "a8245b7cc985a0610d71f9852e9f2767ad1b852c2bdea6f4aadc12cce9c4d6d0" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "385f9776c00dd4a53bdb91ecccec29e9d903fa6352e97b0222301c759c51f9db" "99bc178c2856c32505c514ac04bf25022eaa02e2fddc5e7cdb40271bc708de39" default)))
 '(enwc-wired-device "enp0s25")
 '(enwc-wireless-device "wlp0s26u1u2")
 '(nrepl-message-colors
   (quote
    ("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3")))
 '(package-select ed-packages)
 '(package-selected-packages
   (quote
    (magit slime auto-sudoedit tramp hackernews holiday-pascha-etc smart-mode-line nyan-mode visual-fill-column nasm-mode ivy-youtube ivy-gitlab enwc emms download-region caps-lock auctex amixer ace-window ac-geiser 2048-game)))
 '(pdf-view-midnight-colors (quote ("#DCDCCC" . "#383838"))))

 
 
 
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(Info-quoted ((t (:inherit italic)))))

(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
