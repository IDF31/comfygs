import Graphics.X11.ExtraTypes.XF86
import System.IO
import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.SetWMName
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.UrgencyHook
import XMonad.Layout
import XMonad.Layout.NoBorders
import XMonad.Layout.Grid
import XMonad.Layout.BinarySpacePartition
import XMonad.Prompt
import XMonad.Prompt.Input
import XMonad.Prompt.RunOrRaise
import XMonad.Util.EZConfig
import XMonad.Util.Loggers
import XMonad.Util.Run
import XMonad.Util.Types
import XMonad.Util.Scratchpad
import XMonad.Actions.PhysicalScreens
import XMonad.Actions.NoBorders
import XMonad.Actions.CycleWS
import qualified XMonad.StackSet as W

myBorderWidth = 3

myNormalBorderColor = "#282828"

myFocusedBorderColor = "#33aacc"

myBgColor = "#121212"

myFgColor = "#afafaf"

myBgColor' = "#222222"

myFgColor' = myFocusedBorderColor 

myFont = "xft:\"IBM Plex Sans\":style=Light:size=" ++ myFontSize 

myFontSize = "10"

myTerm = "qterminal"

myShotsDir = "~/Images/Screenshots"

myModMask = mod4Mask

myPP barPipe =
  def
    { ppOutput = hPutStrLn barPipe
    , ppSep = " | "
    , ppCurrent = xmobarColor "cyan" myBgColor . pad
    , ppVisible = xmobarColor "darkcyan" myBgColor . pad
    , ppHidden = xmobarColor "darkgray" myBgColor . pad
    , ppUrgent = xmobarColor "lightred" myBgColor . pad
    , ppTitle = shorten 20
    }

myXmobarRC = "~/.xmonad/xmobarrc"

myXmobarCommand =
  "xmobar " ++ myXmobarRC

myTermOpts = ""

attachToSessionPrompt :: X ()
attachToSessionPrompt = do
  complList <-
    lines <$> runProcessWithInput "tmux" [ "ls", "-F#{session_name}" ] ""
  inputPromptWithCompl myXPConfig "Attach to" (mkComplFunFromList' complList) ?+ \session ->
    runInTerm myTermOpts $ "tmux a -t " ++ session

createSessionPrompt :: X ()
createSessionPrompt =
    inputPrompt myXPConfig "New session name" ?+ \session ->
        runInTerm myTermOpts $ "tmux new -s " ++ session

createSessionWithCommandPrompt :: X ()
createSessionWithCommandPrompt = 
    inputPrompt myXPConfig "New session name" ?+ \session ->
    inputPrompt myXPConfig "New session command" ?+ \command ->
    runInTerm myTermOpts $ "tmux new -s " ++ session ++ " " ++ command

myBrowser = "icecat"
mySearchEngine = "https://ddg.co/html/"

searchWeb :: X ()
searchWeb = 
    inputPrompt myXPConfig "Search" ?+ \query ->
        unsafeSpawn $
        myBrowser ++ " "
        ++ mySearchEngine
        ++ "?q="
        ++ (concat $ map (\x -> if x == ' ' then "%20" else [x]) query)

playInMPV :: X ()
playInMPV = do
    clipboard <- runProcessWithInput "xclip" ["-o"] ""
    inputPromptWithCompl myXPConfig "URL for media" (mkComplFunFromList' [clipboard]) ?+ \url ->
        unsafeSpawn $ "mpv " ++ url


myXPConfig =
  def
    { font = "xft:" ++ myFont ++ ":size=" ++ myFontSize
    , bgColor = myBgColor
    , fgColor = myFgColor
    , bgHLight = myBgColor'
    , fgHLight = myFgColor'
    , position = Top
    , promptBorderWidth = 0
    }

myWorkspaces = [ "I", "II", "III", "IIII", "V", "VI", "VII", "VIII", "VIIII" ]

myKeys =
    [ ("M-p", runOrRaisePrompt myXPConfig)
    , ("M-a", attachToSessionPrompt)
    , ("M-d", createSessionPrompt)
    , ("M-S-d", createSessionWithCommandPrompt)
    , ("M-b", sendMessage ToggleStruts)
    , ("M-S-b", withFocused toggleBorder)
    , ("M-z", moveTo Prev HiddenNonEmptyWS)
    , ("M-x", moveTo Next HiddenNonEmptyWS)
    , ("M-S-z", shiftTo Prev HiddenNonEmptyWS)
    , ("M-S-x", shiftTo Next HiddenNonEmptyWS)
    , ("M-f", shiftTo Next HiddenEmptyWS)
    , ("M-s", searchWeb)
    , ("M-y", playInMPV)
    , ("M-o", onNextNeighbour def W.view)
    , ("M-S-o", onNextNeighbour def W.shift)
    , ("<XF86AudioRaiseVolume>", unsafeSpawn "amixer sset Master 2%+")
    , ("<XF86AudioLowerVolume>", unsafeSpawn "amixer sset Master 2%-")
    , ("<XF86AudioMute>", unsafeSpawn "amixer sset Master toggle")
    , ("<Print>", unsafeSpawn $ "maim " ++ myShotsDir ++ "/$(date +%s).png")
    , ("S-<Print>", unsafeSpawn $ "maim -s " ++ myShotsDir ++ "/$(date +%s).png")
    , ("M-i", scratchpadSpawnActionTerminal myTerm)
    ]
    ++ [(mask ++ "M-" ++ [key], f sc)
    | (key, sc) <- zip "wer" [0..]
    , (f, mask) <- [(viewScreen def, ""), (sendToScreen def, "S-")]]
    ++ [(mask ++ "M-" ++ [key], f w)
      | (key, w) <- zip (concat $ map show [1..]) myWorkspaces
      , (mask, f) <- [ ("", windows . W.view)
                     , ("S-", windows . W.shift)]]

myManageHook = (scratchpadManageHook scratchpadRect <+> 
    composeAll
        [ className =? "feh" --> doFloat
        , className =? "mpv" --> doFloat
        ])
            where
                scratchpadRect = W.RationalRect 0.2 0.3 0.6 0.5

myLayouts = avoidStruts $
            Tall 1 (3/100) (1/2)
            ||| Grid
            ||| emptyBSP
            ||| Full


main = do
  xmproc <- spawnPipe myXmobarCommand
  xmonad
    $ docks
    $ withUrgencyHook NoUrgencyHook
    $ ewmh def
        { borderWidth = myBorderWidth
        , normalBorderColor = myNormalBorderColor
        , focusedBorderColor = myFocusedBorderColor
        , startupHook = setWMName "LG3D"
        , handleEventHook = fullscreenEventHook <+> handleEventHook def
        , logHook = dynamicLogWithPP . myPP $ xmproc
        , manageHook = manageDocks <+> myManageHook
        , layoutHook = myLayouts 
        , terminal = myTerm
        , modMask = myModMask
        , workspaces = myWorkspaces
        } `additionalKeysP` myKeys
