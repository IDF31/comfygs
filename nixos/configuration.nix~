# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "nixos"; # Define your hostname.
  networking.networkmanager.enable = true;  # Enables wireless support via wicd

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # Set your time zone.
  time.timeZone = "Europe/Bucharest";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    wget joe mc htop icecat dzen2 scrot screen irssi aumix ncmpcpp feh mpv
  ];

  # List services that you want to enable:

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Enable urxvtd
  services.urxvtd.enable = true;
  
  # Enable MPD
  services.mpd.enable = true;

  # Enable SSD Trimming
  services.fstrim.enable = true;
  
  # Enable bitlbee
  services.bitlbee.enable = true;
  services.bitlbee.libpurple_plugins = with pkgs; [ purple-matrix
telegram-purple purple-discord ];

  # Enable thinkfan
  services.thinkfan = {
    enable = true;
    sensors = ''
              	hwmon /sys/devices/virtual/thermal/thermal_zone0/temp
              	hwmon /sys/devices/virtual/thermal/thermal_zone1/temp
              '';
    levels = ''
		(0,	0,	64)
		(1,	60,	68)
		(2,	65,	74)
		(3,	70,	82)
		(6,	73,	87)
		(7,	84,	999)
             '';
  };

  # Configure screen
  programs.screen.screenrc = ''
                               vbell off
                             '';

  # Enable and configure Xorg
  services.xserver = {
    enable = true;
    displayManager.slim.enable = true;
    libinput.enable = true;
    windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
    };
    desktopManager.wallpaper.mode = "fill";
    xrandrHeads = [ "VGA-1" { output = "LVDS-1"; primary = true; } ];
  };

  # Set fonts
  fonts.fonts = with pkgs; [
    inconsolata
  ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.idf31 = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" ]; # Enable ‘sudo’ and 'networkmanager' for the user.
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.03"; # Did you read the comment?

}
