# My super epic NixOS configuration

let
  hosts = with builtins; readFile (fetchurl "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts"); # Unified hosts file for ad-blocking
  vim = import ./vim.nix;
  pkgs-overlays = import ./overlays.nix;
  pkgs = import <nixpkgs> { overlays = [ pkgs-overlays ]; };
  categories = with pkgs; {
    utilities = [
      wget
      htop
      vim
      ranger
      home-manager
      nix-prefetch-git
      git
    ];
    development-tools = [
      ghc
      gcc
    ];
  };
in {

  imports =
    [ # Include the results of the hardware scan.
    #<nixos-hardware/lenovo/thinkpad/t430>
    ./linux-configuration.nix
    ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot = {
    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = true;
    supportedFilesystems = [ "zfs" ]; # Allow booting into ZFS
    kernelPackages = pkgs.linuxPackages_latest_hardened; # Use the latest hardened kernel
  };

  networking = {
    hostName = "nixos"; # Define your hostname.
    networkmanager.enable = true;  # Enable NetworkManager
    hostId = "b112ca48"; # Sets the hostId required by ZFS
    extraHosts = hosts; # Set the main host to the unified one above
  };

  # Configure ZFS
  services.zfs = {
    autoScrub.enable = true;
    autoSnapshot = {
      enable = true;
      frequent = 4;
      monthly = 1;
      weekly = 2;
    };
  };

  # Set your time zone.
  time.timeZone = "Europe/Bucharest";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with categories; utilities ++ development-tools;

  # Set global variables for the system
  environment.variables = { EDITOR = "vim"; };

  # Enable upower
  services.upower.enable = true;

  # Enable sway
  programs.sway = {
    enable = true;
    extraPackages = with pkgs; [ qt5.qtwayland xwayland ];
    extraSessionCommands =
      ''
        SDL_VIDEODRIVER="wayland"
        QT_QPA_PLATFORM="wayland-egl"
      '';
   };

  # Set fonts
  fonts.fonts = with pkgs; [
    font-awesome_4
    powerline-fonts
  ];

  # Enable zsh and OhMyZsh
  users.defaultUserShell = pkgs.zsh;
  programs.zsh = {
    enable = true;
    ohMyZsh.enable = true;
    ohMyZsh.theme = "ys";
  };

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Enable opengl and intel video acceleration for ancient thiccpad
  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      (lowPrio vaapiIntel) #when <nixos-hardare> is loaded, don't load this package, as the overlay will cause conflicts
      #intel-media-driver # If you have broadwell or above
    ];
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.idf = {
     isNormalUser = true;
     extraGroups = [ "wheel" "networkmanager" "plugdev" "dialout" ]; # Groups to add the user to
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.03"; # Did you read the comment?

}
