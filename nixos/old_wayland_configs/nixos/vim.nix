# Configure vim system-wide (like a boss)

with import <nixpkgs> {};

vim_configurable.customize {
   name = "vim";

   vimrcConfig = {
    customRC =
      ''
        filetype plugin indent on
        colo jellybeans
        syntax enable
        set expandtab
        set tabstop=4
        set shiftwidth=4
        set nu
        let g:airline_powerline_fonts=1
      '';

    vam.knownPlugins = pkgs.vimPlugins;
    vam.pluginDictionaries = [
      { names = [
          "haskell-vim"
          "hoogle"
          "nerdtree"
          "vim-airline"
          "vim-airline-themes"
          "awesome-vim-colorschemes"
          "vim-haskellConcealPlus"
       ]; }
   ];
 };

}
