# Nixpkgs overriding

self: super: {

  # Patch the intel-vaapi driver so https://github.com/intel/intel-vaapi-driver/issues/419 is fixed
  vaapiIntel = super.vaapiIntel.overrideAttrs (oldAttrs: {
    name = "intel-vaapi-driver-654a9e";
    version = "654a9e";
    src = super.fetchFromGitHub {
      owner = "intel";
      repo = "intel-vaapi-driver";
      rev = "654a9e035b4a43e400dc56da0be786a83886aa76";
      sha256 = "0bvxnbwvmpliqlgbmj2x327329pnbi73h8zx2v4cyz30iyjmib2i";
    };
  });

  # Change the source of home-manager so we always get the latest one
  home-manager = super.home-manager.overrideAttrs (oldAttrs: rec {
    name = "home-manager-master";
    version = "master";
    src = fetchGit "https://github.com/rycee/home-manager";

    installPhase = ''
      install -v -D -m755  ${src}/home-manager/home-manager $out/bin/home-manager
      substituteInPlace $out/bin/home-manager \
        --subst-var-by bash "${self.bash}" \
        --subst-var-by coreutils "${self.coreutils}" \
        --subst-var-by findutils "${self.findutils}" \
        --subst-var-by gnused "${self.gnused}" \
        --subst-var-by less "${self.less}" \
        --subst-var-by HOME_MANAGER_PATH '${src}'
      install -D -m755 ${src}/home-manager/completion.bash \
        $out/share/bash-completion/completions/home-manager
    '';
  });
}
