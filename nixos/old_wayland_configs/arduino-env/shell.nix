with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "arduino-environment";
  buildInputs = with pkgs; [ arduino-core arduino-mk python37Packages.pyserial ];
  shellHook = ''
    export ARDMK_DIR=${pkgs.arduino-mk}
    export ARDUINO_DIR=${pkgs.arduino-core}/share/arduino
    export AVR_TOOLS_DIR=$ARDUINO_DIR/hardware/tools/avr
    '';
}
