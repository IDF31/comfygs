# User configuration

let
  pkgs = import (fetchTarball "https://releases.nixos.org/nixpkgs/nixpkgs-19.09pre188239.c0e56afddbc/nixexprs.tar.xz") {}; # Use the unstable branch for our nix packages instead of the stable branch
  categories = with pkgs; {
    games = [
      gzdoom
      cataclysm-dda-git
    ];
    web = [
      tdesktop
      qutebrowser
      youtube-dl
      deluge
      streamlink
    ];
    media = [
      feh
      cmus
      zathura
    ];
    utilities = [
      neofetch
      grim
      slurp
      speedtest-cli
      i3status-rust
    ];
  };

in {
  # Get us some nice packages for our local usage
  home.packages = with categories; games ++ web ++ media ++ utilities;

  # Add the dotfiles to their respective directory
  home.file = {
    ".config/sway/config".source                     = ./dotfiles/sway/config;
    ".config/i3status-rs/main_monitor.toml".source   = ./dotfiles/i3status-rs/main_monitor.toml;
    ".config/i3status-rs/second_monitor.toml".source = ./dotfiles/i3status-rs/second_monitor.toml;
  };

  # Configure alacritty
  programs.alacritty.enable = true; # TODO: use the alacritty from the unstable branch
  programs.alacritty.settings = {
    font = rec {
      normal.family = "Inconsolata for Powerline";
      bold.family = normal.family;
      italic.family = normal.family;
      size = 10;
    };

    tabspaces = 4;
    window = {
      dynamic_padding = false;
      padding = {
       x = 3;
       y = 3;
     };
   };

    colors = {
      primary = {
        background = "0x121212";
        foreground = "0xeaeaea";
      };

      normal = {
        black = "0x3b3b3b";
        red = "0xcf6a4c";
        green = "0x99ad6a";
        yellow = "0xd8ad4c";
        blue = "0x597bc5";
        magenta = "0xa037b0";
        cyan = "0x71b9f8";
        white = "0xadadad";
      };
    };
  };

  # Configure rofi
  programs.rofi = {
    enable = true; # TODO: same as alacritty oof
    borderWidth = 3;
    theme = "purple";
    font = "Noto Mono for Powerline 9";
  };

  # Configure mpv
  programs.mpv = {
    enable = true; # TODO: same as rofi double oof
    config = rec {
      fullscreen = "yes";
      vo = "gpu";
      gpu-api = "opengl";
      hwdec = "vaapi";
      scale = "lanczos";
      dscale = "mitchell";
      blend-subtitles = "yes";
      gpu-context = "wayland";
      video-sync = "display-resample";
      interpolation = "yes";
      tscale = "oversample";
      display-fps = "60";
      hwdec-codecs = "all";
    };
  };

  # Configure irssi
  programs.irssi = {
    enable = true;
    networks = {
      qoopoo = {
        nick = "idf";
        server = {
          address = "qoopoo.xyz";
          port = 6697;
          autoConnect = true;
        };
      };
    };
  };

  #Configure git 
  programs.git = {
    enable = true;
    ignores = [ "*.swp" ];
    userEmail = "idf31@protonmail.com";
    userName = "IDF31";
  };

  # Configure gtk
  gtk = {
    enable = true;

    iconTheme = {
      package = pkgs.papirus-icon-theme;
      name = "Papirus";
    };

    theme = {
      package = pkgs.arc-theme;
      name = "Arc Theme";
    };

  };

}

