# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let
  pkg_categories = with pkgs; {
    utils = [
      xclip
      xmobar
      lxqt.qterminal
      maim
      unzip
      mc
      htop
      git
      wget
      aumix
      (import ./nvim.nix)
      newsboat ];

    media = [
      mpv
      feh
      cmus
      zathura
      gimp
    ];

    office = [
      rubber
      hovercraft ];

    web = [
      icecat
      weechat
      tdesktop
      qbittorrent ];

    games = [
      (steam.override {
        nativeOnly = true;
        extraPkgs = pkgs: [ at-spi2-core at-spi2-atk ];
      })
      love_11
      openmw
      crawlTiles
      tome4
      cataclysm-dda-git
      (dwarf-fortress.override {
        enableIntro = false;
        enableTruetype = true;
        enableStoneSense = true;
        enableSoundSense = true;
      })];
  };

in
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking = {
    hostName = "nixos"; # Define your hostname.
    networkmanager.enable = true;  # Enables wireless support via NM
    nameservers = ["127.0.0.1"];
  };

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # Set your time zone.
  time.timeZone = "Europe/Bucharest";

  # Configure Nix
  nix = {
    trustedUsers = [ "root" "@wheel" "idf31" ]; # Make my main user a trusted user so cachix works
  };

  # Configure Nixpkgs
  nixpkgs.config = {
    allowUnfree = true;
    allowBroken = true;
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkg_categories;
    utils
    ++ media
    ++ web
    ++ office
    ++ games;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio = {
    enable = true;
    support32Bit = true;
  };
  # Configure qt5
  #qt5 = {
  # enable = true;
  # platformTheme = "gtk2";
  # style = "plastique";
  #};

  # Enable openGL
  hardware.opengl = {
    enable = true;
    driSupport = true;
    driSupport32Bit = true;
    extraPackages = with pkgs;
      [ vaapiIntel libva ];
    extraPackages32 = with pkgs.pkgsi686Linux; [ libva glew ];
  };

  # Enable compton
  services.compton = {
    enable = true;
    backend = "glx";
    vSync = true;
  };

  # Enable urxvtd
  #services.urxvtd = {
  # enable = true;
  # package = pkgs.urxvt_vtwheel;
  # };

  # Enable dnscrypt-proxy
  services.dnscrypt-proxy = {
    enable = true;
    resolverName = "adguard-dns-ns1";
  };

  # Enable fish
  programs.fish = {
    enable = true;
    shellInit = ''
      set -x nixShellsDir "$HOME/nix-shells"

      set -l nix_shell_info (
        if test "$IN_NIX_SHELL" = "1"
          echo -n -s "$nix_shell_info ~>"
        end
      )

      function devEnv --argument envName
        nix-shell "$nixShellsDir/$envName"
      end'';
  };

  # Enable SSD Trimming
  services.fstrim.enable = true;

  # Enable bitlbee
  #services.bitlbee.enable = true;
  #ervices.bitlbee.libpurple_plugins = with pkgs; [ purple-matrix telegram-purple purple-discord ];

  # Enable thinkfan
  services.thinkfan = {
    enable = true;
    sensors = ''
                hwmon /sys/devices/virtual/thermal/thermal_zone0/temp
                hwmon /sys/devices/virtual/thermal/thermal_zone1/temp
          '';
    levels = ''
        (0, 0,  64)
        (1, 60, 68)
        (2, 65, 74)
        (3, 70, 82)
        (6, 80, 90)
        (7, 90, 999)
             '';
  };

  # Enable tmux
  programs.tmux = {
    enable = true;
    keyMode = "vi";
  };

  # Enable and configure Xorg
  services.xserver = {
    enable = true;
    displayManager.lightdm.enable = true;
    libinput.enable = true;
    windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
    };
    desktopManager.wallpaper = {
      mode = "center";
    };

    xrandrHeads =
      [ { output = "VGA-1"; monitorConfig = ''Option "LeftOf" "LVDS-1"''; }
        { output = "LVDS-1"; primary = true; }];

  };

  # Set fonts
  fonts.fonts = with pkgs; [
    noto-fonts
    noto-fonts-emoji
    powerline-fonts
  ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.idf31 = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" ]; # Enable ‘sudo’ and 'networkmanager' for the user.
    shell = "/run/current-system/sw/bin/fish";
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?

}
