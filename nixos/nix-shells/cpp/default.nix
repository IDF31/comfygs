with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "cpp-devel";
  buildInputs = with pkgs; [ cppcheck gcc ];
}
