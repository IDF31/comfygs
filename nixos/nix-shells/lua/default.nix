with import <nixpkgs> {};

stdenv.mkDerivation {
    name = "lua-dev";
    buildInputs = with pkgs;
      [ luajit luajitPackages.luacheck ];
}
