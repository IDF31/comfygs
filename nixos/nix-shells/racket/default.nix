with import <nixpkgs> {};

let 
  racketVim = pkgs.neovim.override {
    configure = {
      customRC = '' syntax on
        filetype indent plugin on 
        set expandtab nu shiftwidth=4 tabstop=4
        colo zenburn 
        if has("autocmd")
          au BufReadPost *.rkt,*.rktl set filetype=scheme
        endif
        set lispwords+=public-method,override-method,private-method,syntax-case,syntax-rules
        '';
        packages.drRacket = with pkgs.vimPlugins; {
          start = [ rainbow_parentheses ale ];
          opt = [ vim-colorschemes ];
        };
      };
    };
in
  stdenv.mkDerivation {
    name = "racketdev";
    buildInputs = with pkgs; [ racket-minimal racketVim ];
  }
