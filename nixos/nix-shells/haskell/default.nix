with import <nixpkgs> {};

let
  ghcEnv = pkgs.haskellPackages.ghcWithPackages (self: [ self.parsec ]);

in      
  stdenv.mkDerivation {
    name = "haskell-dev";
    buildInputs = with pkgs;
      [ ghcEnv haskellPackages.hindent haskellPackages.hdevtools ];
}
