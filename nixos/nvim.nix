with import <nixpkgs> {};
neovim.override {
  configure = {
      customRC = ''
        syntax on
        filetype on
        filetype plugin indent on
        set shiftwidth=2 tabstop=2 nu expandtab
        set mouse=a
        colo jellybeans
        let g:airline_powerline_fonts = 1
        '';

        pathogen = {
          knownPlugins = vimPlugins;
          pluginNames = [
            "vim-nix"
            "haskell-vim"
            "vim-haskellConcealPlus"
            "vim-hdevtools"
            "airline"
            "vim-airline-themes"
            "deoplete-nvim"
            "ale"
            "vim-colorschemes" ];
        };
      };
    }
